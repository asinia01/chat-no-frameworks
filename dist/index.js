function formatDate(date){
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  var formattedDate = new Date(date);
  return formattedDate.toLocaleDateString('en-US', options);
}

function formatAMPM(date) {
  date = new Date(date);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

const ul = document.getElementById('authors');
const url = 'https://api.myjson.com/bins/1geede';
fetch(url)
.then((resp) => resp.json())
.then(function(data) {
  var messages = data.data.messages;
  var date = formatDate(data.data.conversationDate);
  var dateId = document.getElementById('head-date');
  dateId.innerHTML = date;
  var userOne = messages[0].username;
  for(var i = 0; i < messages.length; i++){
    var div = document.createElement("div");
    var img = document.createElement('img');
    var divBubble = document.createElement('div');
    divBubble.classList.add('bubble');
    var head3 = document.createElement('h3');
    var head5 = document.createElement('h5');
    if(messages[i].username == userOne){
      div.classList.add('user-one');
      document.getElementById('messages').appendChild(div);
      img.src = messages[i].image;
      div.appendChild(img);
      divBubble.classList.add('one');
      div.appendChild(divBubble);
    }
    else{
      div.classList.add('user-two');
      document.getElementById('messages').appendChild(div);
      img.src = messages[i].image;
      div.appendChild(img);
      divBubble.classList.add('two');
      div.appendChild(divBubble);
    }
    divBubble.innerHTML = messages[i].message;
    divBubble.appendChild(head3);
    divBubble.appendChild(head5);
    head3.innerHTML = messages[i].username;
    head5.innerHTML = formatAMPM(messages[i].timestamp);

  }
})
.catch(function(error) {
  console.log(error);
});
